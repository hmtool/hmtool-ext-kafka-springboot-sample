package tech.mhuang.ext.kafka.springboot.sample;

import com.alibaba.fastjson.JSON;
import tech.mhuang.ext.kafka.producer.bean.KafkaMsg;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 *
 * 测试消费者
 *
 * @author mhuang
 * @since 1.0.0
 */
@Component("test")
@Slf4j
public class TestConsumer {

    public void invoke(KafkaMsg msg){
        log.info(JSON.toJSONString(msg));
    }
}