package tech.mhuang.ext.kafka.springboot.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 测试用例（用于测试kafka springboot生产消费简单demo）
 *
 * @author mhuang
 * @since 1.0.0
 */
@SpringBootApplication
public class SampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SampleApplication.class, args);
    }
}