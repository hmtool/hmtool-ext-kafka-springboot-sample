package tech.mhuang.ext.kafka.springboot.sample;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import tech.mhuang.ext.kafka.admin.KafkaFramework;
import tech.mhuang.ext.kafka.admin.external.IKafkaProducer;
import tech.mhuang.ext.spring.start.SpringContextHolder;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.Future;


/**
 * 测试生产者
 *
 * @author mhuang
 * @since 1.0.0
 */
@Component
public class TestProducer implements ApplicationRunner {

    @Autowired
    private KafkaFramework kafkaFramework;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        IKafkaProducer kafkaProducer = kafkaFramework.getSuccessProducerMap().get("zhangsan");
        Future<RecordMetadata> metadataFuture = kafkaProducer.send("zhangsan", "name", "zhangsan");
        RecordMetadata metadata = metadataFuture.get();
        System.out.println(metadata);
    }
}